class Service {
    _link = 'https://swapi.dev/api/'

    async getPeoples () {
      const data = await fetch(`${this._link}people/`);
      if (data.status > 300 || data.status < 199){
        throw new Error(`Відбулася помилка в запиті ${data.statusText}: ${data.status}`)
      }
      return await data.json();
    }

    async getPlanets(){
        const data = await fetch(`${this._link}planets/`)
        if (data.status > 300 || data.status < 199){
            throw new Error(`Відбулася помилка в запиті ${data.statusText}: ${data.status}`)
          }
        return await data.json()
    }

    async getStarships(){
        const data = await fetch(`${this._link}starships/`)
        if (data.status > 300 || data.status < 199){
            throw new Error(`Відбулася помилка в запиті ${data.statusText}: ${data.status}`)
          }
        return await data.json()
    }
  }

export default Service;
