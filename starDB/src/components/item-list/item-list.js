import React, { useState, useEffect } from 'react';

import './item-list.css';

const ItemList = ({ service }) => {
  const [items, setItems] = useState(null);

  useEffect(() => {
    service()
      .then((data) => {
        setItems(data.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [service]);

  return (
    <ul className="item-list list-group">
      {items === null ? (
        <span>Загрузка...</span>
      ) : (
        items.map((item, index) => {
          return <li className="list-group-item" key={index * 3 + 'r'}>{item.name}</li>;
        })
      )}
    </ul>
  );
};


export default ItemList;
