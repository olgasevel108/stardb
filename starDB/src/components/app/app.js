import React, {useState, useEffect} from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';




import './app.css';

const App = () => {
  const [test, setTest] = useState(null);

  useEffect(() => {
    new Service().getStarships()
      .then((data) => {
        setTest(data.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <Router>
      <Header />
      <RandomPlanet />

      <div className="row mb2">
        <Routes>
          <Route
            exact
            path="/people"
            element={<ItemList service={() => new Service().getPeoples()} />}
          />
          <Route
            exact
            path="/planets"
            element={<ItemList service={() => new Service().getPlanets()} />}
          />
          <Route
            exact
            path="/starships"
            element={<ItemList service={() => new Service().getStarships()} />}
          />
        </Routes>
        <div className="col-md-6">
          <PersonDetails />
        </div>
      </div>
    </Router>
  );
};

export default App;
